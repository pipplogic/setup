#!/bin/sh
stow --dir=stow --target="$HOME" home --dotfiles

. "$HOME"/.zshenv

stow --no-folding --dir=stow --target="${XDG_CONFIG_HOME:-$HOME/.config}" configs --dotfiles 


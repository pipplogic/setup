set expandtab
set foldlevelstart=99
set foldmethod=syntax
set ignorecase
set mouse=a
set nocursorline
set nojoinspaces
set number
set omnifunc=syntaxcomplete#Complete
"set relativenumber
set runtimepath^=$XDG_CACHE_HOME/nvim,$XDG_CACHE_HOME/nvim/after
set runtimepath^=$XDG_CONFIG_HOME/nvim,$XDG_CONFIG_HOME/nvim/after
set runtimepath^=$XDG_DATA_HOME/nvim,$XDG_DATA_HOME/nvim/after
set shiftwidth=2
set showcmd
set showmatch
set smartcase
set softtabstop=2
set splitbelow
set splitright
set statusline^=%F
set tabstop=2
set undodir^=$XDG_CACHE_HOME/nvim/undo//
set wrap

if !has('nvim')
	set viminfo+=n$XDG_CACHE_HOME/vim/viminfo
endif

" Install vim-plug if missing
if ! filereadable(system('echo -n "${XDG_DATA_HOME:-$HOME/.local/share}/nvim/autoload/plug.vim"'))
	echo "Installing vim-plug..."
	silent !curl -fLo ${XDG_DATA_HOME:-$HOME/.local/share}/nvim/autoload/plug.vim --create-dirs
				\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('$XDG_DATA_HOME/nvim/plugged')

  Plug 'neoclide/coc.nvim', {'branch': 'release'}
  Plug 'junegunn/fzf'
  Plug 'junegunn/fzf.vim'
  Plug 'airblade/vim-gitgutter'
  Plug 'dense-analysis/ale'
  Plug 'pangloss/vim-javascript'
  Plug 'preservim/nerdtree'
  Plug 'sheerun/vim-polyglot'
  Plug 'tpope/vim-sensible'
  Plug 'tpope/vim-surround'
  Plug 'vim-scripts/CycleColor'
  Plug 'tomasr/molokai'
  Plug 'christoomey/vim-tmux-navigator'

call plug#end()

"Auto install plugs on startup
autocmd VimEnter *
      \ if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
      \ | PlugInstall --sync | source $MYVIMRC | q
      \ | endif

"ale
let g:ale_fixers = {
      \ 'javascript': ['prettier', 'eslint'],
      \ }
let g:ale_fix_on_save = 1

nmap ]a <Plug>(ale_next_wrap)
nmap [a <Plug>(ale_previous_wrap)

"coc
let g:coc_data_home='$XDG_DATA_HOME/coc'
let g:coc_global_extensions = [
      \ 'coc-clangd',
      \ 'coc-cmake',
      \ 'coc-css',
      \ 'coc-cssmodules',
      \ 'coc-eslint',
      \ 'coc-git',
      \ 'coc-html',
      \ 'coc-java',
      \ 'coc-json',
      \ 'coc-markdownlint',
      \ 'coc-python',
      \ 'coc-snippets',
      \ 'coc-tailwindcss',
      \ 'coc-texlab',
      \ 'coc-vimlsp',
      \ 'coc-xml',
      \ 'coc-yaml',
      \ ]

"nerdtree
map <C-n> :NERDTreeToggle<CR>
map <leader>n :NERDTreeToggle<CR>

"molokai overrides
colorscheme molokai

hi Normal guibg=NONE ctermbg=NONE
hi LineNr guibg=NONE ctermbg=NONE ctermfg=242
hi SignColumn guibg=NONE ctermbg=NONE
hi Folded guibg=NONE ctermbg=NONE
hi Search ctermbg=179
hi MatchParen ctermfg=166 ctermbg=233


#!/bin/sh

sudo apt update
sudo apt upgrade

sudo apt install docker.io docker-compose -y 
sudo apt install nginx -y
sudo apt install certbot python-certbot-nginx -y
sudo apt install fzf neovim ranger tmux tree -y
sudo apt install git python3 python3-pip -y
sudo apt install guake -y
# sudo apt install handbrake handbrake-cli libdvdcss2 -y
sudo apt install keepassxc -y
sudo apt install obs-studio -y
sudo apt install imagemagick -y
sudo apt install openssh-server -y
sudo apt install scrot -y
sudo apt install shellcheck -y
sudo apt install spotify-client -y
sudo apt install stow -y
# sudo apt install pgadmin3
# sudo apt install rclone -y
sudo apt install vlc -y
sudo apt install zsh -y

yes N | ssh-keygen -t rsa -N '' -f ~/.ssh/id_rsa -q 

ln -s /usr/bin/python3 /usr/bin/python
ln -s /usr/bin/pip3 /usr/bin/pip

sudo usermod --append --groups docker "$USER"
sudo usermod --shell /bin/zsh "$USER"

sh ~/.stow.zsh

mkdir -p "$XDG_CACHE_HOME"/less
mkdir -p "$XDG_CACHE_HOME"/npm
mkdir -p "$XDG_CACHE_HOME"/zsh
mkdir -p "$XDG_DATA_HOME"/node
mkdir -p "$XDG_DATA_HOME"/npm
mkdir -p "$XDG_RUNTIME_DIR"/npm


#Todo: /etc/ssh/sshd_config 
# PasswordAuthentication no
# systemctl restart ssh

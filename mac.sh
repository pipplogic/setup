#!/usr/bin/env bash

#Install Brew
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

cat >> $HOME/.bash_profile <<- 'END'

	# Add brew installed things to bash profile
	[ -f $HOME/.brew_profile] && . $HOME/.brew_profile
END

cat > $HOME/.brew_profile <<- 'END'
	#brew bash completion
	[ -f "$(brew --prefix)/etc/bash_completion.d/brew" ] && \
	  . $(brew --prefix)/etc/bash_completion.d/brew    
END

brew install ffmpeg #video converter
brew install gcc #C/C++ compiler
brew install gpg #Encryption
brew install git #version control
brew install htop #activity monitor
brew install jenv #java version manager
brew install nginx #reverse proxy
brew install nmap #network utils
brew install nvm #node version manager
brew install tmux #terminal multiplexer
brew install w3m #command line browser
brew install youtube-dl #youtube downloader

brew tap AdoptOpenJDK/openjdk # Add additonal java versions

brew cask install adoptopenjdk/openjdk/adoptopenjdk8 #Java 8
#brew cask install adoptopenjdk/openjdk/adoptopenjdk11 #Java 11
brew cask install adoptopenjdk #Latest Java
brew cask install balenaetcher #Disk image writer
brew cask install basictex #Typesetting
brew cask install battle-net #Blizzard games
brew cask install brave-browser #Browser
brew cask install chessx #Chess
brew cask install disk-inventory-x #File size visualizer
brew cask install docker #Container manager
brew cask install electrum #Bitcoin wallet
brew cask install firefox #Browser
brew cask install gimp #Image editor
brew cask install gnucash #finance manager
brew cask install handbrake #Video Utils
brew cask install jetbrains-toolbox #manage IntelliJ/ other editors
brew cask install libreoffice #office suite
brew cask install macpass #kdbx Password manager
brew cask install openemu #game emulator
brew cask install postman #http requests 
brew cask install skitch #screenshots
brew cask install skype #video chat
brew cask install slack #chat
brew cask install spectacle #window manager
brew cask install steam #game store
brew cask install teamviewer #screen sharing
brew cask install thunderbird #Email manager
brew cask install transmission #Torrents
#brew cask install tomcat@8 #web server
brew cask install virtualbox #virtual machine
brew cask install visual-studio-code #code editor
brew cask install vlc #video player

# Set up git
cat >> $HOME/.brew_profile <<- 'END' 

	#git bash completion
	[ -f "$(brew --prefix)/etc/bash_completion.d/git-completion.bash" ] && \
	  . $(brew --prefix)/etc/bash_completion.d/git-completion.bash
END

source $HOME/.brew_profile

cat >> $HOME/.gitignore_global <<- 'END'
	.DS_Store
END

git config --global core.excludesfile $HOME/.gitignore_global

# Set Up nvm
mkdir -p $HOME/.nvm

cat >> $HOME/.brew_profile <<- 'END'

	#nvm setup
	export NVM_DIR="$HOME/.nvm"
	[ -s "/usr/local/opt/nvm/nvm.sh" ] && . "/usr/local/opt/nvm/nvm.sh"  # This loads nvm
	[ -s "/usr/local/opt/nvm/etc/bash_completion" ] && . "/usr/local/opt/nvm/etc/bash_completion"  # This loads nvm bash_completion

	[ -f "$(brew --prefix)/etc/bash_completion.d/nvm" ] && \
	    . $(brew --prefix)/etc/bash_completion.d/nvm

	find-up () {
	    path=$(pwd)
	    while [[ "$path" != "" && ! -e "$path/$1" ]]; do
	        path=${path%/*}
	    done
	  echo "$path"
	}

	usenvm () {
	    if [ ! -s "/usr/local/opt/nvm/nvm.sh" ]; then
	        return;
	    fi
	    nvm_path=$(find-up .nvmrc | tr -d '[:space:]')
	    # If there are no .nvmrc file, use the default nvm version
	    if [[ ! $nvm_path = *[^[:space:]]* ]]; then
	        declare default_version;
	        default_version=$(nvm version default);
	        # If there is no default version, set it to `node`
	        # This will use the latest version on your machine
	        if [[ $default_version == "N/A" ]]; then
	            nvm alias default node;
	            default_version=$(nvm version default);
	        fi
	        # If the current version is not the default version, set it to use the default version
	        if [[ $(nvm current) != "$default_version" ]]; then
	            nvm use default;
	        fi
	        elif [[ -s $nvm_path/.nvmrc && -r $nvm_path/.nvmrc ]]; then
	        declare nvm_version
	        nvm_version=$(<"$nvm_path"/.nvmrc)
	        declare locally_resolved_nvm_version
	        # `nvm ls` will check all locally-available versions
	        # If there are multiple matching versions, take the latest one
	        # Remove the `->` and `*` characters and spaces
	        # `locally_resolved_nvm_version` will be `N/A` if no local versions are found
	        locally_resolved_nvm_version=$(nvm ls --no-colors $(<"./.nvmrc") | tail -1 | tr -d '\->*' | tr -d '[:space:]')
	        # If it is not already installed, install it
	        # `nvm install` will implicitly use the newly-installed version
	        if [[ "$locally_resolved_nvm_version" == "N/A" ]]; then
	            nvm install "$nvm_version";
	        elif [[ $(nvm current) != "$locally_resolved_nvm_version" ]]; then
	            nvm use "$nvm_version";
	        fi
	    fi
	}

	cdnvm(){
	    cd "$@";
	    usenvm;
	}

	usenvm;
	alias cd='cdnvm'
END

source $HOME/.brew_profile
 
nvm install 8 --lts
nvm install 10 --lts
nvm install --lts
nvm install node
nvm alias default lts/*
nvm use default

# Set up jenv
cat >> $HOME/.brew_profile <<- 'END'

	#jenv setup
	export PATH="$HOME/.jenv/bin:$PATH"
	eval "$(jenv init -)"
END

source $HOME/.brew_profile

for dir in $(find /Library/Java/JavaVirtualMachines -iname 'adoptopenjdk*')
do
	jenv add $dir/Contents/Home
done
jenv global 1.8

# Set up tmux
cat >> $HOME/.brew_profile <<- 'END'

	#tmux bash completion
	[ -f "$(brew --prefix)/etc/bash_completion.d/tmux" ] && \
	    . $(brew --prefix)/etc/bash_completion.d/tmux
END

# Set up youtube-dl
cat >> $HOME/.brew_profile <<- 'END'

	#youtube-dl bash completion
	[ -f "$(brew --prefix)/etc/bash_completion.d/youtube-dl.bash-completion" ] && \
	    . $(brew --prefix)/etc/bash_completion.d/youtube-dl.bash-completion
END

source $HOME/.brew_profile

# TODO: No Audacity
# TODO: which google drive?
# TODO: marshmallow player?
# TODO: which logitech?


# Always show scrollbars
# Possible values: `WhenScrolling`, `Automatic` and `Always`
defaults write NSGlobalDomain AppleShowScrollBars -string "Always"
 
# Move Dock
# Possible values: `left`, `right`, and `bottom`
defaults write com.apple.Dock pinning -string left
 
# Expand save panel by default
defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode -bool true
defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode2 -bool true
 
# Expand print panel by default
defaults write NSGlobalDomain PMPrintingExpandedStateForPrint -bool true
defaults write NSGlobalDomain PMPrintingExpandedStateForPrint2 -bool true
 
# Save to disk (not to iCloud) by default
defaults write NSGlobalDomain NSDocumentSaveNewDocumentsToCloud -bool false
 
# Disable Resume system-wide
defaults write com.apple.systempreferences NSQuitAlwaysKeepsWindows -bool false
 
# Disable the crash reporter
defaults write com.apple.CrashReporter DialogType -string "none"
 
# Disable Notification Center and remove the menu bar icon
launchctl unload -w /System/Library/LaunchAgents/com.apple.notificationcenterui.plist 2> /dev/null
 
# Disable automatic substitution
defaults write NSGlobalDomain NSAutomaticPeriodSubstitutionEnabled -bool false
defaults write NSGlobalDomain NSAutomaticCapitalizationEnabled -bool false
defaults write NSGlobalDomain NSAutomaticQuoteSubstitutionEnabled -bool false
 
# TODO: find an image
# Set a custom wallpaper image. `DefaultDesktop.jpg` is already a symlink, and
# all wallpapers are in `/Library/Desktop Pictures/`. The default is `Wave.jpg`.
#rm -rf ~/Library/Application Support/Dock/desktoppicture.db
#sudo rm -rf /System/Library/CoreServices/DefaultDesktop.jpg
#sudo ln -s /path/to/your/image /System/Library/CoreServices/DefaultDesktop.jpg
 
# Disable hibernation (speeds up entering sleep mode)
sudo pmset -a hibernatemode 0
 
# Increase sound quality for Bluetooth headphones/headsets
defaults write com.apple.BluetoothAudioAgent "Apple Bitpool Min (editable)" -int 40
 
# Disable “natural” (Lion-style) scrolling
defaults write NSGlobalDomain com.apple.swipescrolldirection -bool false
 
# TODO: Figure these out
# Trackpad: enable tap to click for this user and for the login screen
# defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad Clicking -bool true
# defaults -currentHost write NSGlobalDomain com.apple.mouse.tapBehavior -int 1
# defaults write NSGlobalDomain com.apple.mouse.tapBehavior -int 1
 
# Trackpad: map bottom right corner to right-click
defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadCornerSecondaryClick -int 2
# defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadRightClick -bool true
# defaults -currentHost write NSGlobalDomain com.apple.trackpad.trackpadCornerClickBehavior -int 1
# defaults -currentHost write NSGlobalDomain com.apple.trackpad.enableSecondaryClick -bool true
 
# Set the timezone; see `sudo systemsetup -listtimezones` for other values
sudo systemsetup -settimezone "America/Chicago" > /dev/null
 
# Set language and text formats
defaults write NSGlobalDomain AppleLanguages -array "en" "es" "de" "fr"
defaults write NSGlobalDomain AppleLocale -string "en_US@currency=USD"
defaults write NSGlobalDomain AppleMeasurementUnits -string "Inches"
defaults write NSGlobalDomain AppleMetricUnits -bool false
 
# Show language menu in the top right corner of the boot screen
sudo defaults write /Library/Preferences/com.apple.loginwindow showInputMenu -bool true
 
# Stop iTunes from responding to the keyboard media keys
launchctl unload -w /System/Library/LaunchAgents/com.apple.rcd.plist 2> /dev/null
 
# Enable subpixel font rendering on non-Apple LCDs
# Reference: https://github.com/kevinSuttle/macOS-Defaults/issues/17#issuecomment-266633501
defaults write NSGlobalDomain AppleFontSmoothing -int 1
 
#### FINDER #####
 
# Use column view in all Finder windows by default
# Four-letter codes for the other view modes: `icnv`, `clmv`, `Flwv`, `Nlsv`
defaults write com.apple.finder FXPreferredViewStyle -string "clmv"
# how hidden files by default
defaults write com.apple.finder AppleShowAllFiles -bool true
# Show the ~/Library folder
chflags nohidden ~/Library
# Show the /Volumes folder
sudo chflags nohidden /Volumes
# show all filename extensions
defaults write NSGlobalDomain AppleShowAllExtensions -bool true
# show status bar
defaults write com.apple.finder ShowStatusBar -bool true
# show path bar
defaults write com.apple.finder ShowPathbar -bool true
# Display full POSIX path as Finder window title
defaults write com.apple.finder _FXShowPosixPathInTitle -bool true
# Keep folders on top when sorting by name
defaults write com.apple.finder _FXSortFoldersFirst -bool true
# When performing a search, search the current folder by default
defaults write com.apple.finder FXDefaultSearchScope -string "SCcf"
# Disable the warning when changing a file extension
defaults write com.apple.finder FXEnableExtensionChangeWarning -bool false
 
# Enable spring loading for directories
defaults write NSGlobalDomain com.apple.springing.enabled -bool true
 
# Reduce the spring loading delay for directories
defaults write NSGlobalDomain com.apple.springing.delay -float 0.2
 
# Avoid creating .DS_Store files on network or USB volumes
defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true
defaults write com.apple.desktopservices DSDontWriteUSBStores -bool true
 
# Disable disk image verification
defaults write com.apple.frameworks.diskimages skip-verify -bool true
defaults write com.apple.frameworks.diskimages skip-verify-locked -bool true
defaults write com.apple.frameworks.diskimages skip-verify-remote -bool true
 
# Automatically open a new Finder window when a volume is mounted
defaults write com.apple.frameworks.diskimages auto-open-ro-root -bool true
defaults write com.apple.frameworks.diskimages auto-open-rw-root -bool true
defaults write com.apple.finder OpenWindowForNewRemovableDisk -bool true
 
# Disable the warning before emptying the Trash
defaults write com.apple.finder WarnOnEmptyTrash -bool false
 
# Expand the following File Info panes:
# “General”, “Open with”, and “Sharing & Permissions”
defaults write com.apple.finder FXInfoPanesExpanded -dict \
	General -bool true \
	OpenWith -bool true \
	Privileges -bool true
 
# Enable highlight hover effect for the grid view of a stack (Dock)
defaults write com.apple.dock mouse-over-hilite-stack -bool true
# Change minimize/maximize window effect
defaults write com.apple.dock mineffect -string "scale"
# Enable spring loading for all Dock items
defaults write com.apple.dock enable-spring-load-actions-on-all-items -bool true
# Show indicator lights for open applications in the Dock
defaults write com.apple.dock show-process-indicators -bool true
 
# TODO: choose arrays
# Wipe all (default) app icons from the Dock
# This is only really useful when setting up a new Mac, or if you don’t use
# the Dock to launch apps.
# defaults write com.apple.dock persistent-apps -array
 
# Don’t animate opening applications from the Dock
defaults write com.apple.dock launchanim -bool false
 
# Remove the auto-hiding Dock delay
# defaults write com.apple.dock autohide-delay -float 0
# Remove the animation when hiding/showing the Dock
# defaults write com.apple.dock autohide-time-modifier -float 0
 
# Automatically hide and show the Dock
# defaults write com.apple.dock autohide -bool true
 
defaults write com.apple.systemuiserver menuExtras -array \
	"/System/Library/CoreServices/Menu Extras/AirPort.menu" \
	"/System/Library/CoreServices/Menu Extras/Battery.menu" \
	"/System/Library/CoreServices/Menu Extras/Bluetooth.menu" \
	"/System/Library/CoreServices/Menu Extras/Clock.menu" \
	"/System/Library/CoreServices/Menu Extras/Volume.menu"
killall SystemUIServer